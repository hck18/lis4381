	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="https://bitbucket.org/hck18/lis4381/src/master/" target="_self"><img src="img/mymelodyicon.png" alt="Bitbucket"/></a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="index.php">LIS4381</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="a1/index.php">A1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="a2/index.php">A2</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="a3/index.php">A3</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="a4/index.php">A4</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="a5/index.php">A5</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="p1/index.php">P1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="p2/index.php">P2</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="skillset14/index.php">Skillset 14</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="skillset15/index.php">Skillset 15</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #98CEFF; font-style: bold;" href="test/index.php">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	
