> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Heath Kwak

#### Assignment 5 Requirements:

* Create a feature the validates the data entry to show an error message or to show if it is successful that it adds to data entries.

#### README.md file should include the following items:

* Screenshot of Invalid Pet Store Name and Error
* Screenshot of Valid Pet Store Name and Successful Entry
* Skillsets 13-15

#### Assignment Screenshots:

[Local Link to LIS4381 Portal](http://localhost/repos/lis4381/index.php)

*Screenshot of Invalid Pet Store Name*:

![LIS4381 Portal Screenshot](img/invalid1.png)

*Screenshot of Invalid Pet Store Name Error Message*:

![LIS4381 Portal Screenshot](img/invalid2.png)

*Screenshot of Valid Pet Store Name*:

![LIS4381 Portal Screenshot](img/valid1.png)

*Screenshot of Valid Pet Store Name Added to Database*:

![LIS4381 Portal Screenshot](img/valid2.png)



Skill Set 13:

![Skillset 13 Screenshot](img/skillset13.png)



Skill Set 14:

![Skillset 14 Screenshot](img/skillset14a.png)

![Skillset 14 Screenshot](img/skillset14b.png)

![Skillset 14 Screenshot](img/skillset14c.png)

![Skillset 14 Screenshot](img/skillset14d.png)



Skill Set 15:

![Skillset 15 Screenshot](img/skillset15a.png)

![Skillset 15 Screenshot](img/skillset15b.png)



