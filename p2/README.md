> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Heath Kwak

#### Project 2 Requirements:

* Add an edit/delete functionality and create an RSS Feed.

#### README.md file should include the following items:

* Screenshot of Before *and* after successful edit 
* Screenshot of Failed validation 
* Screenshot of Delete prompt *and* successfully deleted record 
* Screenshot of RSS feed

#### Assignment Screenshots:

[Local Link to LIS4381 Portal](http://localhost/repos/lis4381/index.php)

*Screenshot of index page for Project 2*:

![LIS4381 Portal Screenshot](img/index1.png)



*Screenshot of Invalid Pet Store Name*:

![LIS4381 Portal Screenshot](img/invalid1.png)

*Screenshot of Invalid Pet Store Name Error Message*:

![LIS4381 Portal Screenshot](img/invalid2.png)

*Screenshot of Valid Pet Store Name Updated*:

![LIS4381 Portal Screenshot](img/valid1.png)

*Screenshot of Delete Record Prompt:*

![LIS4381 Portal Screenshot](img/prompt1.png)

*Screenshot of Successfully Deleted Record:*

![LIS4381 Portal Screenshot](img/delete.png)

*Screenshot of RSS Feed:*

![LIS4381 Portal Screenshot](img/feed.png)