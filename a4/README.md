> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Heath Kwak

#### Assignment 4 Requirements:

* Create an online portfolio using AMPPS

#### README.md file should include the following items:S

* Screenshot of LIS4381 Portal (Main Page)
* Screenshot of Passed Validation
* Screenshot of Failed Validation
* Skillsets 10-12

#### Assignment Screenshots:

[Local Link to LIS4381 Portal](http://localhost/repos/lis4381/index.php)

*Screenshot of LIS4381 Portal (Main Page)*:

![LIS4381 Portal Screenshot](img/portal.png)

Screenshot of Passed Validation:

![Screenshot of Failed Validation](img/passed.png)

Screenshot of Failed Validation:

![Screenshot of Passed Validation](img/failed.png)



| Skill Set 10:                                 | Skill Set 11:                                 | Skill Set 12:                                 |
| --------------------------------------------- | --------------------------------------------- | --------------------------------------------- |
| ![Skillset 10 Screenshot](img/skillset10.png) | ![Skillset 11 Screenshot](img/skillset11.png) | ![Skillset 12 Screenshot](img/skillset12.png) |

