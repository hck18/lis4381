<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Heath Kwak">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Project 1 Requirements:</strong> Screenshot of running application’s first user interface; Screenshot of running application’s second user interface;
 Screenshot of Skill Sets 7-9.
				</p>

				

				<h4>Screenshots of running application’s first and second user interface:</h4>
				<div class="row">
					<div class="column">
							<img src="img/mobile1.png" alt="Concert App First Screen" style="width:100%">
						</div>
						<div class="column">
							<img src="img/mobile2.png" alt="Concert App Second Screen" style="width:100%">
						</div>
						</div>

				<h5> ★ </h5>

				<h4>
					Skill Sets 7-9:</h4>
				<div class="row">
					<div class="column">
						<img src="img/skillsets7.png" alt="Skillset 7" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillsets8.png" alt="Skillset 8" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillsets9.png" alt="Skillset 9" style="width:100%">
					</div>
					</div>

				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
