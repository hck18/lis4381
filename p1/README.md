

# LIS4381 - Mobile Web Application Development

## Heath Kwak

### Project 1 Requirements:

#### README.md file should include the following items:

* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Screenshot of Skill Sets 7-9.

#### Project 1 Screenshots:

| Screenshot of running application’s first user interface: | Screenshot of running application’s second user interface: |
| --------------------------------------------------------- | ---------------------------------------------------------- |
| ![First User Interface Screenshot](img/mobile1.png)       | ![Second User Interface Screenshot](img/mobile2.png)       |

| Skill Set 7:                                 | Skill Set 8:                                 | Skill Set 9:                                 |
| -------------------------------------------- | -------------------------------------------- | -------------------------------------------- |
| ![Skillset 7 Screenshot](img/skillsets7.png) | ![Skillset 8 Screenshot](img/skillsets8.png) | ![Skillset 9 Screenshot](img/skillsets9.png) |


