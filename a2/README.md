> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Heath Kwak

### Assignment 2 Requirements:

- Create a mobile recipe app using Android Studio.

#### README.md file should include the following items:

* Screenshot of running application’s first user interface;

* Screenshot of running application’s second user interface;

* Screenshot of Skill Sets 1-3.


#### Assignment 2 Screenshots:

| Screenshot of running application’s first user interface: | Screenshot of running application’s second user interface: |
| --------------------------------------------------------- | ---------------------------------------------------------- |
| ![First User Interface Screenshot](img/recipe1.png)       | ![Second User Interface Screenshot](img/recipe2.png)       |

| Skill Set 1:                                | Skill Set 2:                                | Skill Set 3:                                |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![Skillset 1 Screenshot](img/skillset1.png) | ![Skillset 2 Screenshot](img/skillset2.png) | ![Skillset 3 Screenshot](img/skillset3.png) |

