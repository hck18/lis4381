<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Content for Assignment 2.">
		<meta name="author" content="Heath Kwak">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> The expected norm...(*be sure* to copy the assignment requirements here!) 
				</p>

				

				<h4>Screenshots of running application’s first and second user interface:</h4>
				<div class="row">
					<div class="column">
							<img src="img/recipe1.png" alt="Recipe App First Screen" style="width:100%">
						</div>
						<div class="column">
							<img src="img/recipe2.png" alt="Recipe App Second Screen" style="width:100%">
						</div>
						</div>

				<h5> ★ </h5>

				<h4>
					Skill Sets 1-3:</h4>
				<div class="row">
					<div class="column">
						<img src="img/skillset1.png" alt="Skillset 1" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillset2.png" alt="Skillset 2" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillset3.png" alt="Skillset 3" style="width:100%">
					</div>
					</div>

				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
