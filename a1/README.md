> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Heath Kwak

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and BitBucket
2. Development Installations
3. Chapter Questions (Chs. 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation

* Screenshot of running java Hello;

* Screenshot of running Android Studio - My First App

* git comments w/ short descriptions;

* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)

  

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - list the files you've changed and those you still need to add or commit
3. git add - add one or more files to staging (index)
4. commit - commit changes to head or any files you added with git
5. git push - send changes to the master branch of your remote
6. git pull - fetch and merge changes on the remote server to your working directory
7. git clone - check out a repository

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/java1.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/hck18/bitbucketstationlocations/ "Bitbucket Station Locations")