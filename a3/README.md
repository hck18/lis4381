

# LIS4381 - Mobile Web Application Development

## Heath Kwak

### Assignment 3 Requirements:

#### README.md file should include the following items:

* Screenshot of ERD;
* Screenshot of 10 records for each table
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;
* Screenshot of Skill Sets 4-6.
* Links to the following files:
      * a3.mwb
      * a3.sql

#### Assignment 3 Screenshots:

*Screenshot of ERD*

![ERD Screenshot](img/a3_erd.png)



*Screenshot of 10 records for pet store table*

![petstore records](img/petstore_record.png)

*Screenshot of 10 records for pet table*

![petstore records](img/pet_record.png)

*Screenshot of 10 records for customer table*

![petstore records](img/customer_record.png)

| Screenshot of running application’s first user interface: | Screenshot of running application’s second user interface: |
| --------------------------------------------------------- | ---------------------------------------------------------- |
| ![First User Interface Screenshot](img/event1.png)        | ![Second User Interface Screenshot](img/event2.png)        |

| Skill Set 4:                                | Skill Set 5:                                | Skill Set 6:                                |
| ------------------------------------------- | ------------------------------------------- | ------------------------------------------- |
| ![Skillset 4 Screenshot](img/skillset4.png) | ![Skillset 5 Screenshot](img/skillset5.png) | ![Skillset 6 Screenshot](img/skillset6.png) |

*Links to a3.mwb and a3.sql*

[a3.mwb](docs/a3.mwb)

[a3.sql](docs/a3.sql)