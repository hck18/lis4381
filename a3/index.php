<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Content for Assignment 3.">
		<meta name="author" content="Heath Kwak">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Assignment 3 Requirements:</strong> Screenshot of ERD; Screenshot of 10 records for each table; Screenshot of running application’s first user interface;
					 Screenshot of running application’s second user interface; Screenshot of Skill Sets 4-6; Links to the following files: a3.mwb, a3.sql
				</p>

				

				<h4>Screenshots of running application’s first and second user interface:</h4>
				<div class="row">
					<div class="column">
							<img src="img/event1.png" alt="Concert App First Screen" style="width:100%">
						</div>
						<div class="column">
							<img src="img/event2.png" alt="Concert App Second Screen" style="width:100%">
						</div>
						</div>

				<h4>Screenshot of ERD</h4>
				<img src="img/a3_erd.png" class="img-responsive center-block" alt="ERD Screenshot">
				<h4>Pet Store Records</h4>
				<img src="img/petstore_record.png" class="img-responsive center-block" alt="Pet Store Records">
				<h4>Pet Records</h4>
				<img src="img/pet_record.png" class="img-responsive center-block" alt="Pet Records">
				<h4>Customer Records</h4>
				<img src="img/customer_record.png" class="img-responsive center-block" alt="Customer Records">

				<h5> ★ </h5>

				<h4>
					Skill Sets 4-6:</h4>
				<div class="row">
					<div class="column">
						<img src="img/skillset4.png" alt="Skillset 4" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillset5.png" alt="Skillset 5" style="width:100%">
					</div>
					<div class="column">
						<img src="img/skillset6.png" alt="Skillset 6" style="width:100%">
					</div>
					</div>

				
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
