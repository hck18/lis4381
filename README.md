> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Heath Kwak

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

       * Install AMPPS
       * Install JDK
       * Install Android Studio and create "My First App"
       * Provide screenshots of installations
       * Create Bitbucket repo
       * Complete Bitbucket tutorials (bitbucketstationlocations)
       * Provide git command descriptions

   

2. [A2 README.md](a2/README.md "My A2 README.md file")

       * Create "Healthy Recipes" Android App
       * Skill Sets 1-3

   

3. [A3 README.md](a3/README.md "My A3 README.md file")

      * Create ERD based upon business rules

      * Provide screenshot of completed ERD

      * Provide DB resource links

      * Screenshots of "Concert" Mobile Application

      *  Skill Sets 4-6

        

4. [A4 README.md](a4/README.md "My A4 README.md file")

      * Screenshot of LIS4381 Portal (Main Page)

      * Screenshot of Passed Validation and Failed Validation

      * Skill Sets 10-12

        

5. [A5 README.md](a5/README.md "My A5 README.md file")

       * Screenshot of Invalid Pet Store Name and Error
       * Screenshot of Valid Pet Store Name and Successful Entry
       * Skill Sets 13-15

6. [P1_README.md](p1/README.md "My P1 README.md file")

       * Screenshots of "Business Card" Application
       *  Skill Sets 7-9

   

7. [P2 README.md](p2/README.md "My P2 README.md file")

      * Screenshot of Before and after successful edit

      * Screenshot of Failed validation
      
      * Screenshot of Delete Prompt and successfully deleted record
      
      * Screenshot of RSS Feed
      
        
